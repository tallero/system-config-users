7-10-2001 - Brent Fox `<bfox@redhat.com>`

system-config-users relies on libuser.  Some things don't currently work in libuser, such as adding additional users to existing groups.  Since they don't work in libuser, they don't work in system-config-users.  This should get sorted out soon.

12-02-2020 - tallero `<echo cGVsbGVncmlub3ByZXZldGVAZ21haWwuY29tCg== | base64 -d>`

The source code [repository](https://web.archive.org/web/20150921154923/https://git.fedorahosted.org/git/system-config-users.git) reported on Fedora [wiki](https://fedoraproject.org/wiki/SystemConfig/users) is dead and has not been moved to [Pagure](https://pagure.io/system-config-users) apparently, so there is no commit history.
